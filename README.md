# README #

This repository host the documentations for the engineering tools developped at Fisher & Paykel Appliance in PD Dunedin.

### mxTools ###

This is a set of tools for post-processing the data files created by the Yogogawa data-loggers.
See the documentation at: <https://pddunedin.bitbucket.io/mxtools/>.